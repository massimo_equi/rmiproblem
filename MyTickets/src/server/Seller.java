package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import interfaces.TicketSeller;

public class Seller extends UnicastRemoteObject implements TicketSeller{
	private static final long serialVersionUID = 1L;
	private int currTicket;
	
	public Seller() throws RemoteException{
		this.currTicket = 1;
	}
	
	public synchronized int buyTicket() throws RemoteException{
		return this.currTicket++;
	}
}
