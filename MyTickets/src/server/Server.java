package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import interfaces.TicketSeller;

public class Server {
	public static void main(String[] args) throws RemoteException{
		TicketSeller ts = new Seller();
		int PORT = Integer.parseInt(args[1]);
		String URL = args[0];
		Registry reg = LocateRegistry.createRegistry(PORT);
		
		System.setProperty("java.rmi.server.hostname", URL);//URL = indirizzo trasporto del server
		reg.rebind("TicketServer", ts);
		System.out.println("Server ready at " + URL + " on port "+ PORT);
	}
}
