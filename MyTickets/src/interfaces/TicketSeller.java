package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TicketSeller extends Remote{
	public int buyTicket() throws RemoteException;
}
