package client;

import interfaces.TicketSeller;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Client {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
		String URL = args[0], PORT = args[1];
		String srv = "rmi://" + URL + ":" + PORT + "/TicketServer";
		

		System.out.println("Client attempt connection to: "+srv);
		
		TicketSeller ts = (TicketSeller) Naming.lookup(srv);
		
		System.setProperty("java.rmi.server.hostname", "192.168.5.2");
		System.out.println("ticket bought: " + ts.buyTicket());
	}
}
